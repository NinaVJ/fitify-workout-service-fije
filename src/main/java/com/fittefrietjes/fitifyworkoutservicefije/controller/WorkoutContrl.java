package com.fittefrietjes.fitifyworkoutservicefije.controller;

import com.fittefrietjes.fitifyworkoutservicefije.model.Workout;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/workout")
public class WorkoutContrl {

    @GetMapping("/workoutId")
    public Workout getWorkoutById(@PathVariable("workoutId") int workoutId){
        var workout = new Workout();
        workout.setWorkoutId(workoutId);
        return workout;
    }
}
