package com.fittefrietjes.fitifyworkoutservicefije;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitifyWorkoutServiceFijeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitifyWorkoutServiceFijeApplication.class, args);
	}

}
